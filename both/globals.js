Collections = {};

var imageStore = new FS.Store.GridFS("images");

Collections.Images = new FS.Collection("images", {
  stores: [imageStore]
});

Collections.Post = new Mongo.Collection("post");