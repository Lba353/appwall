"use strict";

Template.posts.helpers({
  imageList: function (createdAt) {
    return Collections.Images.find({}, {
      sort: {createdAt: -1}
    });
  },
  
  postList: function (createdAt) {
    return Collections.Post.find({}, {
      sort: {createdAt: -1}
    });
  },
  
  imageDisplay: function (fileObject) {
    var picture = Collections.Images.find({_id: this.image_id}).fetch();
    return picture[0].url();
  },
  
  updateMasonry: function () {
    $(".grid").imagesLoaded().done(function () {
      $(".grid").masonry({
        itemSelector: ".grid-item",
        columnWidth: ".grid-sizer",
        percentPosition: true,
        gutter: 10
      });
    });
  }
  
});